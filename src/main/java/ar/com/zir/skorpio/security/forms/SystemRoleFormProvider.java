/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.forms;

import ar.com.zir.skorpio.core.config.DbSystemConfig;
import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.forms.FormProvider;
import ar.com.zir.skorpio.core.ui.forms.SkorpioAbmField;
import ar.com.zir.skorpio.core.ui.forms.SkorpioBeanItemContainer;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListColumn;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListForm;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import ar.com.zir.skorpio.security.api.SystemRole;
import ar.com.zir.skorpio.security.services.SkorpioSecurityService;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author jmrunge
 */
@Singleton
@LocalBean
public class SystemRoleFormProvider extends FormProvider {
    @Inject
    private SkorpioSecurityService securityService;
    @Inject
    private SystemOptions systemOptions;
    @Inject
    private DbSystemConfig config;

    @Override
    public List<SkorpioListColumn> getFormListColumns() {
        List<SkorpioListColumn> columns = new ArrayList<>();
        columns.add(new SkorpioListColumn("roleName", String.class, "Rol"));
        columns.add(new SkorpioListColumn("description", String.class, "Descripción"));
        return columns;
    }

    @Override
    public SkorpioBeanItemContainer getFormContainer() {
        return new SkorpioBeanItemContainer(SystemRole.class);
    }

    @Override
    public void doRemoveItem(Object item, SystemUser user) throws PersistenceException {
        securityService.removeSystemRole((SystemRole)item);
    }

    @Override
    public Class getObjectClass() {
        return SystemRole.class;
    }

    @Override
    public Page getTableObjects() {
        return securityService.getPagedSystemRoles(config.getConfigIntegerValue("default.cant.rows"));
    }

    @Override
    public List<SkorpioAbmField> getFormFields() {
        List<SkorpioAbmField> fields = new ArrayList<>();
        fields.add(new SkorpioAbmField("Nombre", "roleName", true));
        fields.add(new SkorpioAbmField("Descripción", "description", true, new String[] {"longtext"}));
        return fields;
    }

    @Override
    public Object getNewObject() {
        return securityService.getNewSystemRole();
    }

    @Override
    public Object doCreateObject(Object item, SystemUser user) throws PersistenceException {
        return securityService.createSystemRole((SystemRole)item);
    }

    @Override
    public Object doUpdateObject(Object item, SystemUser user) throws PersistenceException {
        return securityService.updateSystemRole((SystemRole)item);
    }

    @Override
    public SkorpioPersistedForm getFormInstance(String action) {
        switch (action) {
            case SystemOptions.ACTION_LIST:
                return new SkorpioListForm();
            case SystemOptions.ACTION_ADD:
            case SystemOptions.ACTION_EDIT:
                SystemRoleForm form = new SystemRoleForm();
                form.setSystemUsers(securityService.getSystemUsers());
                form.setSystemOptions(systemOptions.getSecuritySystemOptions());
                return form;
            default:
                return null;
        }
    }

    @Override
    public Object getFreshObject(Object item) throws PersistenceException {
        return securityService.refreshSystemRole((SystemRole)item);
    }
    
}
