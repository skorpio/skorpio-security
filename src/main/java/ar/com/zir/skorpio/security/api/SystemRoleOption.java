/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.api;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmrunge
 */
@Entity
@TableGenerator(allocationSize = 1, 
        name = "SystemRoleOptionGenerator", 
        pkColumnValue = "SystemRoleOption")
public class SystemRoleOption implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "SystemRoleOptionGenerator")
    private Integer id;
    @NotNull
    @Size(min=4, max=40)
    private String systemOption;
    @NotNull
    @ManyToOne
    private SystemRole systemRole;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSystemOption() {
        return systemOption;
    }

    public void setSystemOption(String systemOption) {
        this.systemOption = systemOption;
    }

    public SystemRole getSystemRole() {
        return systemRole;
    }

    public void setSystemRole(SystemRole systemRole) {
        this.systemRole = systemRole;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemRoleOption other = (SystemRoleOption) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "ar.com.zir.skorpio.security.api.SystemRoleOption[ id=" + id + " ]";
    }
    
}
