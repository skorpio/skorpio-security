/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.api;

import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmrunge
 */
@NamedQueries({
    @NamedQuery(name = "SystemRole.findByRoleName", query = "select sr from SystemRole sr where sr.roleName = :roleName"),
    @NamedQuery(name = "SystemRole.findAll", query = "select sr from SystemRole sr")
})
@Entity
@TableGenerator(allocationSize = 1, 
        name = "SystemRoleGenerator", 
        pkColumnValue = "SystemRole")
public class SystemRole implements Serializable, SynchronizableObject {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "SystemRoleGenerator")
    private Integer id;
    @NotNull(message="Debe ingresar el nombre del rol")
    @Size(min=4, max=20, message="El nombre del rol debe ser de entre 4 y 20 caracteres")
    private String roleName;
    @NotNull
    @Size(min=4, max=40, message="La descripción debe ser de entre 4 y 40 caracteres")
    private String description;
    @OneToMany(mappedBy = "systemRole", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval = true)
    private List<SystemUserRole> systemUserRoles;
    @OneToMany(mappedBy = "systemRole", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval = true)
    private List<SystemRoleOption> systemRoleOptions;
    @Transient
    private List<SystemUserRole> usersToRemove;

    public SystemRole() {
        systemUserRoles = new ArrayList<>();
        systemRoleOptions = new ArrayList<>();
        usersToRemove = new ArrayList<>();
    }
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SystemUserRole> getSystemUserRoles() {
        return systemUserRoles;
    }

    public void setSystemUserRoles(List<SystemUserRole> systemUserRoles) {
        if (systemUserRoles != null) {
            this.systemUserRoles.stream().filter((sur) -> (!systemUserRoles.contains(sur))).forEach((sur) -> {
                if (!usersToRemove.contains(sur)) 
                    usersToRemove.add(sur);
            });
            systemUserRoles.stream().filter((sur) -> (usersToRemove.contains(sur))).forEach((sur) -> {
                usersToRemove.remove(sur);
            });
        } else {
            this.systemUserRoles.stream().forEach((sur) -> {
                if (!usersToRemove.contains(sur))
                    usersToRemove.add(sur);
            });
        }
        this.systemUserRoles = systemUserRoles;
    }

    public List<SystemRoleOption> getSystemRoleOptions() {
        return systemRoleOptions;
    }

    public void setSystemRoleOptions(List<SystemRoleOption> systemRoleOptions) {
        if (this.systemRoleOptions != null) {
            for (SystemRoleOption sro : this.systemRoleOptions) {
                sro.setSystemRole(null);
            }
            this.systemRoleOptions.clear();
        }
        this.systemRoleOptions = systemRoleOptions;
        if (this.systemRoleOptions != null) {
            for (SystemRoleOption sro : this.systemRoleOptions) {
                sro.setSystemRole(this);
            }
        }
    }
    
    public void addSystemUserRole(SystemUserRole sur) {
        sur.setSystemRole(this);
        systemUserRoles.add(sur);
    }

    public List<SkorpioSystemUser> getSystemUsers() {
        if (systemUserRoles == null) return null;
        List<SkorpioSystemUser> systemUsers = new ArrayList<>();
        systemUserRoles.stream().forEach((sur) -> {
            systemUsers.add(sur.getSystemUser());
        });
        return systemUsers;
    }

    public void setSystemUsers(List<SkorpioSystemUser> systemUsers) {
        if (systemUsers == null) {
            setSystemUserRoles(null);
        } else {
            List<SystemUserRole> users = new ArrayList<>();
            for (SkorpioSystemUser user : systemUsers) {
                SystemUserRole sur = null;
                for (SystemUserRole tmp : systemUserRoles) {
                    if (tmp.getSystemUser().equals(user)) {
                        sur = tmp;
                        break;
                    }
                }
                if (sur == null) {
                    sur = new SystemUserRole();
                    sur.setSystemRole(this);
                    sur.setSystemUser(user);
                }
                users.add(sur);
            }
            setSystemUserRoles(users);
        }
    }

    public List<SystemUserRole> getUsersToRemove() {
        return usersToRemove;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemRole other = (SystemRole) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return roleName;
    }

    @Override
    public Class getMainClass() {
        return getClass();
    }
    
}
