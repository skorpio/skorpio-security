/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.api;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmrunge
 */
@Entity
@TableGenerator(allocationSize = 1, 
        name = "SystemUserRolesGenerator", 
        pkColumnValue = "SystemUserRoles")
public class SystemUserRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "SystemUserRolesGenerator")
    private Integer id;
    @NotNull
    @ManyToOne
    private SkorpioSystemUser systemUser;
    @NotNull
    @ManyToOne
    private SystemRole systemRole;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SkorpioSystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SkorpioSystemUser systemUser) {
        this.systemUser = systemUser;
    }

    public SystemRole getSystemRole() {
        return systemRole;
    }

    public void setSystemRole(SystemRole systemRole) {
        this.systemRole = systemRole;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemUserRole other = (SystemUserRole) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.zir.skorpio.security.api.SystemUserRoles[ id=" + id + " ]";
    }
    
}
