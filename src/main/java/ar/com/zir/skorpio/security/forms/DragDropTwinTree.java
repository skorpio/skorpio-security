/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.forms;

import com.vaadin.data.Property;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.ClientSideCriterion;
import com.vaadin.event.dd.acceptcriteria.SourceIs;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author jmrunge
 */
public class DragDropTwinTree extends CustomField<Collection> {
    private HierarchicalContainer sourceContainer;
    private Tree source;
    private Tree target;
    private Panel sourcePanel;
    private Panel targetPanel;
    private Panel wrapperPanel;
    private ItemConverter itemConverter;
    boolean ignoreChange;
    
    public DragDropTwinTree(HierarchicalContainer sourceContainer) {
        this.sourceContainer = sourceContainer;
        init();
    }
    
    private void init() {
        if (sourceContainer.getItemIds().isEmpty()) {
            sourceContainer.addItem("");
            sourceContainer.setChildrenAllowed("", false);
        }
        
        source = new Tree("Available", sourceContainer);
        source.setDragMode(Tree.TreeDragMode.NODE);
        source.setSelectable(false);
        
        HierarchicalContainer targetContainer = new HierarchicalContainer();
        if (targetContainer.getItemIds().isEmpty()) {
            targetContainer.addItem("");
            targetContainer.setChildrenAllowed("", false);
        }
        
        target = new Tree("Assigned", targetContainer);
        target.setDragMode(Tree.TreeDragMode.NODE);
        target.setSelectable(false);
        
        source.setDropHandler(getDropHandler(new SourceIs(target)));
        target.setDropHandler(getDropHandler(new SourceIs(source)));
    
        sourcePanel = new Panel("Available");
        targetPanel = new Panel("Assigned");
        wrapperPanel = new Panel();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        if (readOnly) {
            source.setDragMode(Tree.TreeDragMode.NONE);
            target.setDragMode(Tree.TreeDragMode.NONE);
        } else {
            source.setDragMode(Tree.TreeDragMode.NODE);
            target.setDragMode(Tree.TreeDragMode.NODE);
        }
    }
    
    @Override
    protected Component initContent() {
        GridLayout layout = new GridLayout(2, 1);
        layout.setMargin(true);
        layout.setSpacing(true);
        
        sourcePanel.setContent(source);
        sourcePanel.addStyleName("plain");
        
        targetPanel.setContent(target);
        targetPanel.addStyleName("plain");
        
        layout.addComponent(sourcePanel);
        layout.addComponent(targetPanel);

        wrapperPanel.setContent(layout);
        return wrapperPanel;    
    }

    @Override
    public Class<? extends Collection> getType() {
        return Collection.class;
    }
    
    public void addSourceStyleName(String styleName) {
        source.addStyleName(styleName);
        sourcePanel.addStyleName(styleName);
    }
    
    public void addTargetStyleName(String styleName) {
        target.addStyleName(styleName);
        targetPanel.addStyleName(styleName);
    }
    
    @Override
    public void addStyleName(String styleName) {
        addStyleName(styleName, true);
    }
    
    public void addStyleName(String styleName, boolean wrapperOnly) {
        super.addStyleName(styleName);
        addWrapperStyleName(styleName);
        if (!wrapperOnly) {
            addSourceStyleName(styleName);
            addTargetStyleName(styleName);
        }
    }
    
    public void removeSourceStyleName(String styleName) {
        source.removeStyleName(styleName);
        sourcePanel.removeStyleName(styleName);
    }
    
    public void removeTargetStyleName(String styleName) {
        target.removeStyleName(styleName);
        targetPanel.removeStyleName(styleName);
    }
    
    @Override
    public void removeStyleName(String styleName) {
        removeStyleName(styleName, true);
    }
    
    public void removeStyleName(String styleName, boolean wrapperOnly) {
        super.removeStyleName(styleName);
        removeWrapperStyleName(styleName);
        if (!wrapperOnly) {
            removeSourceStyleName(styleName);
            removeTargetStyleName(styleName);
        }
    }
    
    public void addWrapperStyleName(String styleName) {
        wrapperPanel.addStyleName(styleName);
    }
    
    public void removeWrapperStyleName(String styleName) {
        wrapperPanel.removeStyleName(styleName);
    }
    
    @Override
    public void setCaption(String caption) {
        wrapperPanel.setCaption(caption);
    }
    
    public void setSourceCaption(String caption) {
        source.setCaption(caption);
        sourcePanel.setCaption(caption);
    }
    
    public void setTargetCaption(String caption) {
        target.setCaption(caption);
        targetPanel.setCaption(caption);
    }
    
    @Override
    public void setPropertyDataSource(Property newDataSource) {
        super.setPropertyDataSource(newDataSource);
        resetValues();
        initValues((Collection) newDataSource.getValue());
        super.addValueChangeListener((Property.ValueChangeEvent event) -> {
            if (!ignoreChange) {
                resetValues();
                initValues((Collection) event.getProperty().getValue());
            }
        });
    }
    
    public void setItemConverter(ItemConverter itemConverter) {
        this.itemConverter = itemConverter;
    }
    
    private void resetValues() {
        List targetItems = new ArrayList();
        for (Object id : target.getContainerDataSource().getItemIds()) {
            if (!target.hasChildren(id))
                targetItems.add(id);
        }
        for (Object item : targetItems) {
            addItemToTarget(item, (HierarchicalContainer) target.getContainerDataSource(), source);
        }
    }
    
    private void initValues(Collection newValues) {
        for (Object obj : newValues) {
            if (itemConverter != null)
                obj = itemConverter.convertObjectToItem(obj);
            if (obj != null)
                addItemToTarget(obj, sourceContainer, target, false);
        }
    }
    
    private void updateValues() {
        ignoreChange = true;
        setValue(getValue());
        ignoreChange = false;
    }

    @Override
    public List getValue() {
        List values = new ArrayList();
        for (Object item : target.getContainerDataSource().getItemIds()) {
            Object obj = item;
            if (itemConverter != null)
                obj = itemConverter.convertItemToObject(item);
            if (obj != null)
                values.add(obj);
        }
        return values;
    }
    
    private DropHandler getDropHandler(ClientSideCriterion acceptCriterion) {
        return new DropHandler() {

            @Override
            public void drop(DragAndDropEvent event) {
                final DataBoundTransferable t = (DataBoundTransferable) event.getTransferable();
                final HierarchicalContainer sourceContainer = (HierarchicalContainer) t.getSourceContainer();
                Tree target = (Tree) event.getTargetDetails().getTarget();
                final Object sourceItem = t.getItemId();
                addItemToTarget(sourceItem, sourceContainer, target);
                updateValues();
            }

            @Override
            public AcceptCriterion getAcceptCriterion() {
                return new And(acceptCriterion, AbstractSelect.AcceptItem.ALL);
            }
            
        };
    }
    
    private void addItemToTarget(Object sourceItem, HierarchicalContainer sourceContainer, Tree target) {
        addItemToTarget(sourceItem, sourceContainer, target, true);
    }
    
    private void addItemToTarget(Object sourceItem, HierarchicalContainer sourceContainer, Tree target, boolean expand) {
        HierarchicalContainer targetContainer = (HierarchicalContainer) target.getContainerDataSource();
        if (sourceItem.equals("")) {
            //is the fake node, dont pay attention to it
            return;
        }
        //add it
        addItemIfCorresponds(sourceItem, sourceContainer, targetContainer);
        addItemChildrenIfCorresponds(sourceItem, sourceContainer, targetContainer);

        //removes it from source
        removeWithParentsIfCorresponds(sourceItem, sourceContainer);
        if (expand) {
            //expands parents
            expandParents(sourceItem, targetContainer, target);
            target.expandItemsRecursively(sourceItem);
        }
        //check for fake node
        if (sourceContainer.getItemIds().isEmpty()) {
            sourceContainer.addItem("");
            sourceContainer.setChildrenAllowed("", false);
        }
        targetContainer.removeItem("");
    }

    private void addItemIfCorresponds(Object sourceItem, HierarchicalContainer sourceContainer, HierarchicalContainer targetContainer) {
        if (!targetContainer.containsId(sourceItem)) {
            targetContainer.addItem(sourceItem);
            if (sourceContainer.getParent(sourceItem) != null) {
                Object parent = sourceContainer.getParent(sourceItem);
                addItemIfCorresponds(parent, sourceContainer, targetContainer);
                targetContainer.setParent(sourceItem, parent);
            }
        }
    }

    private void addItemChildrenIfCorresponds(Object sourceItem, HierarchicalContainer sourceContainer, HierarchicalContainer targetContainer) {
        boolean hasChildren = false;
        if (sourceContainer.getChildren(sourceItem) != null) {
            for (Object child : sourceContainer.getChildren(sourceItem)) {
                hasChildren = true;
                if (!targetContainer.containsId(child)) 
                    targetContainer.addItem(child);
                targetContainer.setParent(child, sourceItem);
                addItemChildrenIfCorresponds(child, sourceContainer, targetContainer);
            }
        }
        targetContainer.setChildrenAllowed(sourceItem, hasChildren);
    }

    private void removeWithParentsIfCorresponds(Object sourceItem, HierarchicalContainer sourceContainer) {
        Object parent = sourceContainer.getParent(sourceItem);
        sourceContainer.removeItemRecursively(sourceItem);
        if (parent != null) {
            if (!sourceContainer.hasChildren(parent))
                removeWithParentsIfCorresponds(parent, sourceContainer);
        }
    }

    private void expandParents(Object targetItem, HierarchicalContainer targetContainer, Tree targetTree) {
        if (targetContainer.getParent(targetItem) != null) {
            Object parent = targetContainer.getParent(targetItem);
            targetTree.expandItem(parent);
            expandParents(parent, targetContainer, targetTree);
        }
    }
    
    public interface ItemConverter {
        public Object convertObjectToItem(Object obj);
        public Object convertItemToObject(Object item);
    }
}
