/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.forms;

import ar.com.zir.skorpio.core.config.DbSystemConfig;
import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.forms.FormProvider;
import ar.com.zir.skorpio.core.ui.forms.SkorpioAbmField;
import ar.com.zir.skorpio.core.ui.forms.SkorpioBeanItemContainer;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListColumn;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListForm;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import ar.com.zir.skorpio.security.api.SkorpioSystemUser;
import ar.com.zir.skorpio.security.api.SystemRole;
import ar.com.zir.skorpio.security.services.SkorpioSecurityService;
import com.vaadin.ui.Notification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author jmrunge
 */
@Singleton
@LocalBean
public class SystemUserFormProvider extends FormProvider {
    @Inject
    private SkorpioSecurityService securityService;
    @Inject
    private DbSystemConfig config;

    @Override
    public List<SkorpioListColumn> getFormListColumns() {
        List<SkorpioListColumn> columns = new ArrayList<>();
        columns.add(new SkorpioListColumn("userName", String.class, "Nombre de Usuario"));
        columns.add(new SkorpioListColumn("roles", String.class, "Roles"));
        return columns;
    }

    @Override
    public SkorpioBeanItemContainer getFormContainer() {
        return new SkorpioBeanItemContainer(SkorpioSystemUser.class);
    }

    @Override
    public void doRemoveItem(Object item, SystemUser user) throws PersistenceException {
        securityService.removeSystemUser((SkorpioSystemUser) item);
    }

    @Override
    public Class getObjectClass() {
        return SkorpioSystemUser.class;
    }

    @Override
    public Page getTableObjects() {
        return securityService.getPagedSystemUsers(config.getConfigIntegerValue("default.cant.rows"));
    }

    @Override
    public List<SkorpioAbmField> getFormFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getNewObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object doCreateObject(Object item, SystemUser user) throws PersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object doUpdateObject(Object item, SystemUser user) throws PersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SkorpioPersistedForm getFormInstance(String action) {
        switch (action) {
            case SystemOptions.ACTION_LIST:
                SkorpioListForm listForm = new SkorpioListForm();
                addListFormAsListener(listForm, SystemRole.class, this::updateListForm);
                return listForm;
//            case MilongaOptions.ACTION_ADD:
//            case MilongaOptions.ACTION_EDIT:
//                return new SystemRoleForm();
            default:
                return null;
        }
    }

    @Override
    public Object getFreshObject(Object item) throws PersistenceException {
        return securityService.refreshSystemUser((SkorpioSystemUser) item);
    }
    
    private void updateListForm(SkorpioPersistedForm form, Object obj) {
        try {
            Map<Integer, SkorpioSystemUser> roles = new HashMap<>();
            ((SystemRole) obj).getSystemUsers().stream().forEach((user) -> {
                roles.put(user.getId(), user);
            });
            SystemRole role = securityService.refreshSystemRole((SystemRole) obj);
            role.getSystemUsers().stream().forEach((user) -> {
                roles.remove(user.getId());
                roles.put(user.getId(), user);
            });
            roles.keySet().stream().forEach((key) -> {
                ((SkorpioListForm)form).updateItem(roles.get(key));
            });
        } catch (PersistenceException ex) {
            Logger.getLogger(SystemUserFormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing role", Notification.Type.ERROR_MESSAGE);
        }
    }
    
}
