/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.security.api;

import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmrunge
 */
@NamedQueries({
    @NamedQuery(name = "SkorpioSystemUser.findByUserName", query = "select su from SkorpioSystemUser su where su.userName = :userName"),
    @NamedQuery(name = "SkorpioSystemUser.findAll", query = "select su from SkorpioSystemUser su")
})
@Entity
@TableGenerator(allocationSize = 1, 
        name = "SystemUserGenerator", 
        pkColumnValue = "SystemUser")
@Inheritance(strategy = InheritanceType.JOINED)
public class SkorpioSystemUser implements SystemUser, Serializable, SynchronizableObject {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "SystemUserGenerator")
    private Integer id;
    @NotNull(message="Debe ingresar el nombre de usuario")
    @Size(min=4, max=10, message="El nombre de usuario debe ser de entre 4 y 10 caracteres")
    private String userName;
//    @NotNull(message="Debe ingresar la contraseña")
    @Size(min=6, max=10, message="La contraseña debe ser de entre 6 y 10 caracteres")
    @Transient
    private String password;
    @OneToMany(mappedBy = "systemUser", cascade = CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval = true)
    private List<SystemUserRole> systemUserRoles;
    @Column(columnDefinition = "varbinary(255)")
    private byte[] pwd;
    @Transient
    private List<SystemUserRole> rolesToRemove;
    
    public SkorpioSystemUser() {
        systemUserRoles = new ArrayList<>();
        rolesToRemove = new ArrayList<>();
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    public List<SystemUserRole> getSystemUserRoles() {
        return systemUserRoles;
    }

    public void setSystemUserRoles(List<SystemUserRole> systemUserRoles) {
        if (systemUserRoles != null) {
            this.systemUserRoles.stream().filter((sur) -> (!systemUserRoles.contains(sur))).forEach((sur) -> {
                if (!rolesToRemove.contains(sur)) 
                    rolesToRemove.add(sur);
            });
            systemUserRoles.stream().filter((sur) -> (rolesToRemove.contains(sur))).forEach((sur) -> {
                rolesToRemove.remove(sur);
            });
        } else {
            this.systemUserRoles.stream().forEach((sur) -> {
                if (!rolesToRemove.contains(sur))
                    rolesToRemove.add(sur);
            });
        }
        this.systemUserRoles = systemUserRoles;
    }

    public byte[] getPwd() {
        return pwd;
    }

    public void setPwd(byte[] pwd) {
        this.pwd = pwd;
    }

    public void addSystemUserRole(SystemUserRole sur) {
        sur.setSystemUser(this);
        systemUserRoles.add(sur);
    }

    public List<SystemRole> getSystemRoles() {
        if (systemUserRoles == null) return null;
        List<SystemRole> systemRoles = new ArrayList<>();
        systemUserRoles.stream().forEach((sur) -> {
            systemRoles.add(sur.getSystemRole());
        });
        return systemRoles;
    }

    public void setSystemRoles(List<SystemRole> systemRoles) {
        if (systemRoles == null) {
            setSystemUserRoles(null);
        } else {
            List<SystemUserRole> roles = new ArrayList<>();
            for (SystemRole role : systemRoles) {
                SystemUserRole sur = null;
                for (SystemUserRole tmp : systemUserRoles) {
                    if (tmp.getSystemRole().equals(role)) {
                        sur = tmp;
                        break;
                    }
                }
                if (sur == null) {
                    sur = new SystemUserRole();
                    sur.setSystemRole(role);
                    sur.setSystemUser(this);
                }
                roles.add(sur);
            }
            setSystemUserRoles(roles);
        }
    }

    public List<SystemUserRole> getRolesToRemove() {
        return rolesToRemove;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SkorpioSystemUser other = (SkorpioSystemUser) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String getStringRepresentation() {
        return getUserName();
    }

    @Override
    public void parseStringRepresentation(String representation) {
        setUserName(representation);
    }
    
    @Override
    public String toString() {
        return getUserName();
    }
    
    public String getRoles() {
        if (systemUserRoles != null) {
            StringBuilder bld = new StringBuilder();
            systemUserRoles.stream().forEach((sur) -> {
                bld.append(sur.getSystemRole().toString());
                bld.append(" | ");
            });
            if (bld.length() > 0)
                bld.delete(bld.length() - 2, bld.length());
            return bld.toString();
        }
        return null;
    }
    
    @Override
    public Class getMainClass() {
        return getClass();
    }
    
}
