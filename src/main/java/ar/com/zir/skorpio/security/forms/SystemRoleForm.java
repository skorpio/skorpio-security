/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.forms;

import ar.com.zir.skorpio.core.options.SystemOption;
import ar.com.zir.skorpio.core.ui.components.DragDropTwinCol;
import ar.com.zir.skorpio.core.ui.forms.SkorpioAbmForm;
import ar.com.zir.skorpio.security.api.SkorpioSystemUser;
import ar.com.zir.skorpio.security.api.SystemRoleOption;
import ar.com.zir.skorpio.security.forms.DragDropTwinTree.ItemConverter;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author jmrunge
 */
public class SystemRoleForm extends SkorpioAbmForm {
    private Collection<SkorpioSystemUser> users;
    private Collection<SystemOption> options;
    private DragDropTwinCol twinCol;
    private DragDropTwinTree twinTree;
    
    public void setSystemUsers(Collection<SkorpioSystemUser> users) {
        this.users = users;
    }
    
    public void setSystemOptions(Collection<SystemOption> options) {
        this.options = options;
    }

    @Override
    protected void bindAdditionalFields(FieldGroup binder) {
        super.bindAdditionalFields(binder);
        grid.setRows(3);
        grid.addComponent(getTwinCol(binder), 0, 1, 1, 1);
        grid.addComponent(getTwinTree(binder), 2, 0, 2, 2);
    }
    
    @Override
    protected void enableEditing() {
        super.enableEditing();
        twinCol.setReadOnly(false);
        twinCol.removeStyleName("visible");
        twinTree.setReadOnly(false);
        twinTree.removeStyleName("visible");
    }
    
    @Override
    protected void disableEditing() {
        super.disableEditing();
        twinCol.setReadOnly(true);
        twinCol.addStyleName("visible");
        twinTree.setReadOnly(true);
        twinTree.addStyleName("visible");
    }

    private Component getTwinCol(FieldGroup binder) {
        twinCol = new DragDropTwinCol(SkorpioSystemUser.class, "userName", users);
        twinCol.addStyleName("visible");
        twinCol.setCaption("Usuarios");
        twinCol.setListColumnWidth(143);
        twinCol.setSourceListCaption("Disponibles");
        twinCol.setTargetListCaption("Asignados");
        binder.bind(twinCol, "systemUsers");
        twinCol.setReadOnly(true);
        return twinCol;
    }
    
    private Component getTwinTree(FieldGroup binder) {
        HierarchicalContainer sourceContainer = new HierarchicalContainer();
        addTreeOptions(sourceContainer, options, null);
        twinTree = new DragDropTwinTree(sourceContainer);
        twinTree.setItemConverter(new ItemConverter() {
            @Override
            public Object convertObjectToItem(Object obj) {
                if (obj instanceof SystemRoleOption)
                    return new ActionDAO(((SystemRoleOption)obj).getSystemOption());
                else
                    return null;
            }

            @Override
            public Object convertItemToObject(Object item) {
                if (item instanceof ActionDAO) {
                    SystemRoleOption sro = new SystemRoleOption();
                    sro.setSystemOption(((ActionDAO)item).getActionForSave());
                    return sro;
                } else {
                    return null;
                }
            }
        });
        twinTree.addStyleName("visible");
        twinTree.setCaption("Opciones");
        twinTree.setSourceCaption("Disponibles");
        twinTree.setTargetCaption("Asignadas");
        binder.bind(twinTree, "systemRoleOptions");
        twinTree.setReadOnly(true);
        return twinTree;
    }

    private void addTreeOptions(HierarchicalContainer container, Collection<SystemOption> opts, SystemOption parent) {
        opts.stream().forEach(option -> {
            container.addItem(option);
            if (parent != null)
                container.setParent(option, parent);
            if (option.getChildren() != null && !option.getChildren().isEmpty()) {
                addTreeOptions(container, option.getChildren(), option);
            } else {
                if (option.getActions() != null) {
                    option.getActions().keySet().stream().forEach((act) -> {
                        ActionDAO dao = new ActionDAO(option.getId(), act);
                        container.addItem(dao);
                        container.setParent(dao, option);
                        container.setChildrenAllowed(dao, false);
                    });
                }
            }
        });
    }
    
    private class ActionDAO {
        private String parent;
        private String action;

        public ActionDAO(String savedAction) {
            String[] tmp = savedAction.split("\\|");
            this.parent = tmp[0];
            this.action = tmp[1];
        }
        
        public ActionDAO(String parent, String action) {
            this.parent = parent;
            this.action = action;
        }

        public String getParent() {
            return parent;
        }

        public void setParent(String parent) {
            this.parent = parent;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 61 * hash + Objects.hashCode(this.parent);
            hash = 61 * hash + Objects.hashCode(this.action);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ActionDAO other = (ActionDAO) obj;
            if (!Objects.equals(this.parent, other.parent)) {
                return false;
            }
            return Objects.equals(this.action, other.action);
        }

        @Override
        public String toString() {
            return action;
        }
        
        public String getActionForSave() {
            return parent + "|" + action;
        }
    }
    
}
