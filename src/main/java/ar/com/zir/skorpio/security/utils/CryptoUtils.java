/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.security.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author jmrunge
 */
public class CryptoUtils {
    private static final SecretKey SECRET = new SecretKeySpec(new BigInteger("2e71150d702ac553b89e5d294e761ce7", 16).toByteArray(), "AES");
    private static final String CHARSET = "UTF-8";
    
    public static String generateKey() {
        try {
            KeyGenerator gen = KeyGenerator.getInstance("AES");
            gen.init(128);
            SecretKey k = gen.generateKey();
            return new BigInteger(k.getEncoded()).toString(16);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(CryptoUtils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static byte[] getEncryptedData(String data) {
        try {
            byte[] encryptedConfig = encodeAESCBC(data);
            return encodeAES(encryptedConfig);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(CryptoUtils.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }
    
    private static byte[] encodeAESCBC(String data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        c.init(Cipher.ENCRYPT_MODE, SECRET);
        
        byte[] encryptedData = c.doFinal(data.getBytes(CHARSET));
        log("AES-CBC ENC DATA", encryptedData);
        byte[] iv = c.getIV();
        log("AES-CBC IV", iv);
        byte[] finalData = new byte[iv.length + encryptedData.length];
        for (int i = 0; i < finalData.length; i++) {
            if (i < 16)
                finalData[i] = iv[i];
            else
                finalData[i] = encryptedData[i - 16];
        }
        log("AES-CBC FINAL ENC DATA", finalData);
        return finalData;
    }
    
    private static byte[] encodeAES(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        log("AES DATA", data);
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, SECRET);
        byte[] encryptedData = c.doFinal(data);
        log("AES ENC DATA", encryptedData);
        return encryptedData;
    }
    
    public static String getDecryptedData(byte[] encryptedData) {
        try {
            log("ENC DATA", encryptedData);
            byte[] data = decodeAES(encryptedData);
            log("DEC DATA", data);
            byte[] iv = new byte[16];
            byte[] configData = new byte[data.length - 16];
            for (int i = 0; i < data.length; i++) {
                if (i < 16)
                    iv[i] = data[i];
                else
                    configData[i - 16] = data[i];
            }
            log("IV", iv);
            log("ENC CONFIG", configData);
            byte[] decodedData = decodeAESCBC(iv, configData);
            return new String(decodedData, CHARSET);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(CryptoUtils.class.getName()).log(Level.SEVERE, null, ex);
            return new String();
        }    
    }
    
    private static byte[] decodeAES(byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        log("AES ENC DATA", data);
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, SECRET);
        byte[] decryptedData = c.doFinal(data);
        log("AES DEC DATA", decryptedData);
        return decryptedData;
    }
    
    private static byte[] decodeAESCBC(byte[] iv, byte[] configData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        log("AES-CBC IV", iv);
        log("AES-CBC ENC DATA", configData);
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        c.init(Cipher.DECRYPT_MODE, SECRET, new IvParameterSpec(iv));
        byte[] decryptedConfig = c.doFinal(configData);
        return decryptedConfig;
    }
    
    private static void log(String title, byte[] data) throws UnsupportedEncodingException {
//        System.out.println(title);
//        System.out.println(new String(data, CHARSET));
//        System.out.println("LENGTH: " + data.length);
    }
}
