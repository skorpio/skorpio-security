/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.security.services;

import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.security.SecurityService;
import ar.com.zir.skorpio.core.sync.SkorpioSyncService;
import ar.com.zir.skorpio.core.sync.SyncException;
import ar.com.zir.skorpio.security.api.SkorpioSystemUser;
import ar.com.zir.skorpio.security.api.SystemRole;
import ar.com.zir.skorpio.security.api.SystemUserRole;
import ar.com.zir.skorpio.security.utils.CryptoUtils;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jmrunge
 */
@LocalBean
@Stateless
public class SkorpioSecurityService extends SecurityService {
    @PersistenceContext
    private EntityManager em;
    @Inject
    private SkorpioSyncService syncService;
    
    @Override
    public SkorpioSystemUser login(SystemUser su) {
        SkorpioSystemUser ssu = getSystemUserByUserName(su.getUserName());
        if (ssu != null && su.getPassword().equals(CryptoUtils.getDecryptedData(ssu.getPwd()))) 
            return ssu;
        else
            return null;
    }

    @Override
    public SkorpioSystemUser getNewSystemUser() {
        return new SkorpioSystemUser();
    }
    
    private SkorpioSystemUser getSystemUserByUserName(String userName) {
        try {
            return (SkorpioSystemUser) em.createNamedQuery("SkorpioSystemUser.findByUserName").setParameter("userName", userName).getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    public List<SkorpioSystemUser> getSystemUsers() {
        return em.createNamedQuery("SkorpioSystemUser.findAll").getResultList();
    }
    
    public Page getPagedSystemUsers(int pageSize) {
        BigInteger total = (BigInteger) em.createNativeQuery("select count(*) from SkorpioSystemUser").getSingleResult();
        return new Page(em, "SkorpioSystemUser.findAll", 0, pageSize, total.intValue());
    }
    
    public SkorpioSystemUser createSystemUser(SkorpioSystemUser su) {
        su.setPwd(CryptoUtils.getEncryptedData(su.getPassword()));
        em.persist(su);
        em.flush();
        return su;
    }
    
    public SkorpioSystemUser updateSystemUser(SkorpioSystemUser user) throws PersistenceException {
        try {
            if (user.getPassword() != null)
                user.setPwd(CryptoUtils.getEncryptedData(user.getPassword()));
            else if (user.getPwd() != null)
                user.setPassword(CryptoUtils.getDecryptedData(user.getPwd()));
            for (SystemUserRole sur : user.getRolesToRemove()) {
                SystemRole role = em.find(SystemRole.class, sur.getSystemRole().getId());
                syncService.lockObject(role, user, UUID.randomUUID().toString());
                role.getSystemUserRoles().remove(sur);
                sur.setSystemRole(null);
                sur.setSystemUser(null);
                em.merge(role);
                syncService.releaseObject(role, user);
                em.remove(em.find(SystemUserRole.class, sur.getId()));
            }
            SkorpioSystemUser newUser = em.merge(user);
            em.flush();
            newUser.setPassword(null);
            return newUser;
        } catch (SyncException e) {
            throw new PersistenceException("Error locking SystemUser", e);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error actualizando el usuario", e);
            throw new PersistenceException("Error actualizando el usuario", e);
        }
    }
    
    public void removeSystemUser(SkorpioSystemUser user) throws PersistenceException {
        try {
            em.remove(em.find(SkorpioSystemUser.class, user.getId()));
            em.flush();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error eliminando el usuario", e);
            throw new PersistenceException("Error eliminando el usuario", e);
        }
    }

    public SkorpioSystemUser refreshSystemUser(SkorpioSystemUser user) throws PersistenceException {
        try {
            user = em.find(SkorpioSystemUser.class, user.getId());
            em.refresh(user);
            return user;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error refrescando el usuario", ex);
            throw new PersistenceException("Error refrescando el usuario", ex);
        }
    }

    public List<SystemRole> getSystemRoles() {
        return em.createNamedQuery("SystemRole.findAll").getResultList();
    }
    
    public Page getPagedSystemRoles(int pageSize) {
        BigInteger total = (BigInteger) em.createNativeQuery("select count(*) from SystemRole").getSingleResult();
        return new Page(em, "SystemRole.findAll", 0, pageSize, total.intValue());
    }
    
    public Object getNewSystemRole() {
        return new SystemRole();
    }
    
    public SystemRole createSystemRole(SystemRole role) throws PersistenceException {
        try {
            em.persist(role);
            em.flush();
            return role;
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error creando el rol", e);
            throw new PersistenceException("Error creando el rol", e);
        }
    }
    
    public SystemRole updateSystemRole(SystemRole role) throws PersistenceException {
        try {
            for (SystemUserRole sur : role.getUsersToRemove()) {
                SkorpioSystemUser su = em.find(SkorpioSystemUser.class, sur.getSystemUser().getId());
                syncService.lockObject(su, su, UUID.randomUUID().toString());
                su.getSystemUserRoles().remove(sur);
                sur.setSystemRole(null);
                sur.setSystemUser(null);
                em.merge(su);
                syncService.releaseObject(su, su);
                em.remove(em.find(SystemUserRole.class, sur.getId()));
            }
            SystemRole newRole = em.merge(role);
            em.flush();
            return newRole;
        } catch (SyncException e) {
            throw new PersistenceException("Error locking SystemRole", e);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error actualizando el rol", e);
            throw new PersistenceException("Error actualizando el rol", e);
        }
    }
    
    public void removeSystemRole(SystemRole role) throws PersistenceException {
        try {
            em.remove(em.find(SystemRole.class, role.getId()));
            em.flush();
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error eliminando el rol", e);
            throw new PersistenceException("Error eliminando el rol", e);
        }
    }
    
    public SystemRole refreshSystemRole(SystemRole role) throws PersistenceException {
        try {
            role = em.find(SystemRole.class, role.getId());
            em.refresh(role);
            return role;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error refrescando el rol", ex);
            throw new PersistenceException("Error refrescando el rol", ex);
        }
    }

}
